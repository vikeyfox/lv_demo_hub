/*
 * @Author: Flying
 * @Date: 2022-07-24 17:00:09
 * @LastEditors: Flying
 * @LastEditTime: 2022-07-24 21:15:50
 * @Description: 修改文件
 */
#pragma once
#include "../../my_conf.h"
#include "../../lvgl/lvgl.h"
#include <iostream>
#include <vector>

using sys_quick_func_t = lv_obj_t *(*)(lv_obj_t *);

class sys_quick_win
{
private:
    bool is_show;
    int start_x;
    int start_y;
    int end_x;
    int end_y;
    int w;
    int h;
    lv_obj_t *cont;
    lv_obj_t *win;
    static void exit_evnet_cb(lv_event_t *e);
    static void open_changer_anim_cb(void *var, int32_t v);
    static void exit_changer_anim_cb(void *var, int32_t v);
    void calc_end_pos();
    ~sys_quick_win();

public:
    sys_quick_win(int start_x, int start_y, sys_quick_func_t func);
    void del_this();
};
